<!-- sidebar -->
<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Dashboard</div>
                <a @click="menu=0" class="nav-link" href="#">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Inicio
                </a>
                <div class="sb-sidenav-menu-heading">Opciones</div>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-hand-holding-usd"></i></div>Cobros
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a @click="menu=1" class="nav-link" href="#">Florida</a>
                        <a @click="menu=2" class="nav-link" href="#">San Fancisco</a>
                    </nav>
                </div>
                <a @click="menu=3" class="nav-link" href="#">
                    <div class="sb-nav-link-icon"><i class="fas fa-cash-register"></i></div>Pagos
                </a>

                <a @click="menu=4" class="nav-link" href="#">
                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>Clientes
                </a>
                <a @click="menu=5" class="nav-link" href="#">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>Gráficos
                </a>

                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                    <div class="sb-nav-link-icon"><i class="fas fa-cog"></i></div>
                    Gestionar
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a @click="menu=8" class="nav-link" href="#">
                            Sedes
                        </a>
                        <a class="nav-link" href="#">
                            Pagos Fijos
                        </a>
                        <a class="nav-link" href="#">
                            Antenas
                        </a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseAccess" aria-expanded="false" aria-controls="collapseAccess">
                    <div class="sb-nav-link-icon"><i class="fas fa-key"></i></div>
                    Acceso
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseAccess" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link" href="#">
                            Usuarios
                        </a>
                        <a class="nav-link" href="#">
                            Roles
                        </a>
                    </nav>
                </div>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Accediste como:</div>
            Admin
        </div>
    </nav>
</div>
