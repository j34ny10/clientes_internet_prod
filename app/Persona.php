<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //protected $table = 'personas';
    protected $fillable = [
        'nombres',
        'celular',
        'email',
        'direccion',
        'fecha_registro',
        'fecha_cobros_pagos',
        'monto_mensual'
    ];
    public $timestamps = false;
}
