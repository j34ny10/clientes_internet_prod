<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Persona;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //if (!$request->ajax()) return redirect('/');
        $buscar=$request->buscar;
        $criterio=$request->criterio;
        if($buscar==''){
            $clientes = Cliente::join('personas','clientes.id','=','personas.id')
            ->select('clientes.id','clientes.idSede','clientes.tipoCliente','clientes.tieneMiAntena',
            'clientes.deuda','clientes.esMoroso','clientes.condicion',
            'personas.id','personas.nombres','personas.celular')
            ->orderBy('clientes.id', 'desc')->paginate(3);
            $clientes=Cliente::orderBy('id','desc')->paginate(4);
        }else{
            $clientes=Cliente::where($criterio,'like','%'.$buscar.'%')
                                ->orderBy('id','desc')
                                ->paginate(3);
        }

        //$clientes = Cliente::paginate(2);
        return[
            'pagination' => [
                'total'        => $clientes->total(),
                'current_page' => $clientes->currentPage(),
                'per_page'     => $clientes->perPage(),
                'last_page'    => $clientes->lastPage(),
                'from'         => $clientes->firstItem(),
                'to'           => $clientes->lastItem(),
            ],
            'clientes'=>$clientes
        ];
        
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        
        try{
            DB::beginTransaction();
            $persona = new Persona();
            $persona->nombres = $request->nombres;
            $persona->celular = $request->celular;
            $persona->email = $request->email;
            $persona->direccion = $request->direccion;
            $persona->fecha_registro = $request->fecha_registro;
            $persona->fecha_cobros_pagos = $request->fecha_cobros_pagos;
            $persona->monto_mensual = $request->monto_mensual;
            $persona->save();

            $cliente = new Cliente();
            $cliente->id = $persona->id;
            $cliente->idSede = $request->idSede;
            $cliente->tipoCliente = $request->tipoCliente;
            //$cliente->tieneMiAntena = $request->tieneMiAntena;
            $cliente->deuda = 0;
            //$cliente->esMoroso = $request->esMoroso;
            //$cliente->condicion = $request->condicion;
            $cliente->save();

            DB::commit();

        } catch (Exception $e){
            DB::rollBack();
        }

        
        
    }

    
}
