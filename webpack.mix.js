const mix = require('laravel-mix');


mix.styles([
    'resources/assets/carpeta/css/app.css',
    'resources/assets/carpeta/css/styles.css',
 ], 'public/css/estilos.css')
 .scripts([
    'resources/assets/carpeta/js/jquery-3.4.1.min.js',
    'resources/assets/carpeta/js/popper.min.js',
    'resources/assets/carpeta/js/all.js',
    'resources/assets/carpeta/js/bootstrap.js',
    'resources/assets/carpeta/js/scripts.js',
 ], 'public/js/estilos.js')
.js('resources/js/app.js', 'public/js');
